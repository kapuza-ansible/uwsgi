# Ubuntu uwsgi install
## Install
### Python
```bash
ansible ubuntu -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-pip;"
```

### Ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```
### Download role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/uwsgi.git
  name: uwsgi
EOF
echo "roles/ubuntu" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Example of use
```bash
cat <<'EOF' > uwsgi.yml
- name: Use uwsgi role
  hosts:
    uwsgi
  tasks:
    - name: Uwsgi install
      include_role:
        name: uwsgi
EOF
ansible-playbook uwsgi.yml
```
